//
//  ToDoListApp.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 19.09.2023.
//

import SwiftUI

/*
 MVVM Architecture
 Model      - data point
 View       - UI
 ViewModel  - Manages models for view

 */

@main
struct ToDoListApp: App {
    @StateObject var listVM: ListViewModel = ListViewModel()
    var body: some Scene {
        WindowGroup {
            NavigationStack {
                ListView()
            }
            .environmentObject(listVM)
        }
    }
}
