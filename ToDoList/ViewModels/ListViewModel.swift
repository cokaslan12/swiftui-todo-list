//
//  ListViewModel.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 20.09.2023.
//

import Foundation

/*
 CRUD FUNCTIONS
 
 Create
 Read
 Update
 Delete
 */

class ListViewModel: ObservableObject{
    //MARK: DEFINED
    @Published var items:[ItemModel] = []{
        //MARK: DIDSET
        didSet{
            saveItems()
        }
    }
    
    let db:UserDefaults  = UserDefaults.standard
    let itemsKey:String = "items"
    
    //MARK: INITIALIZER
    init(){
        getItems()
    }
    
    //MARK: GET ITEMS FROM DB
    func getItems(){
        guard 
            let data = db.data(forKey: self.itemsKey),
            let newItems = try? JSONDecoder().decode([ItemModel].self, from: data)
        else { return }
        
        self.items = newItems
    }
    
    //MARK: FUNCTIONS
    func removeItem(indexSet:IndexSet){
        items.remove(atOffsets: indexSet)
    }
    
    func moveItem(indexSet:IndexSet, indices:Int){
        items.move(fromOffsets: indexSet, toOffset: indices)
    }
    
    func addItem(title:String){
        let newItem = ItemModel(title: title, isCompleted: false)
        self.items.append(newItem)
    }
    
    func updateItem(item:ItemModel){
        if let index = self.items.firstIndex(where: {$0.id == item.id}){
            self.items[index] = item.updateCompletion()
        }
    }
    
    func saveItems(){
        if let encodedData = try? JSONEncoder().encode(items){
            db.setValue(encodedData, forKey: self.itemsKey)
        }
    }
    
    
    
}
