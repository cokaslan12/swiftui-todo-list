//
//  AddView.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 19.09.2023.
//

import SwiftUI

struct AddView: View {
    @Binding var isPresented:Bool
    @EnvironmentObject var listVM: ListViewModel
    @State var textField:String = ""
    @FocusState var focusInItem:Bool
    @State var alertTitle:String = ""
    @State var showAlert:Bool = false
    var body: some View {
        ScrollView {
            VStack(spacing:10) {
                TextField("Type something here...", text: $textField)
                    .focused($focusInItem)
                    .padding(.leading)
                    .frame(height: 55)
                    .background(Color(UIColor.secondarySystemBackground))
                    .cornerRadius(10)
                Button (action: saveButtonPressed, label: {
                    Text("Save".uppercased())
                        .foregroundColor(.white)
                        .font(.headline)
                        .frame(height: 55)
                        .frame(maxWidth: .infinity)
                        .background(Color.accentColor)
                        .cornerRadius(10)
                })
                
            }
            .padding(16)
        }
        .navigationTitle("Add Item 🖋️")
        .onAppear {
            self.focusInItem = true
        }
        .alert(self.alertTitle, isPresented: self.$showAlert) {
            Button("Okey") {
                self.showAlert.toggle()
            }
        }
    }
    
    func saveButtonPressed(){
        if textIsCheck(){
            listVM.addItem(title: self.textField)
            self.isPresented.toggle()
        }
    }
    
    func textIsCheck()->Bool{
        if self.textField.count < 3{
            self.alertTitle = "Item area should be non empty"
            self.showAlert.toggle()
            return false
        }
        return true
    }
}
