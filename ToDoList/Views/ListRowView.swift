//
//  ListRowView.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 19.09.2023.
//

import SwiftUI

struct ListRowView: View {
    let item: ItemModel
    var body: some View {
        HStack {
            Image(systemName: item.isCompleted ? "checkmark.circle" : "circle")
                .foregroundColor(item.isCompleted ? .green : .red)
            Text("\(item.title)")
            Spacer()
        }
        .font(.title2)
        .padding(.vertical, 8)
    }
}

struct ListRowView_Previews: PreviewProvider {
    static var previews: some View {
            ListRowView(item: ItemModel(title: "Hello", isCompleted: false))
    }
}
