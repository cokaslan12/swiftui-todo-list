//
//  ListView.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 19.09.2023.
//

import SwiftUI

struct ListView: View {
    @State var isPresented:Bool = false
    @EnvironmentObject var listVM: ListViewModel
    var body: some View {
       
        ZStack(){
            if listVM.items.isEmpty{
                NoItemsView()
                    .transition(AnyTransition.opacity.animation(.easeIn))
            }
            
            if !listVM.items.isEmpty{
                List {
                    ForEach(listVM.items) {
                        item in
                        ListRowView(item: item)
                            .onTapGesture {
                                withAnimation(.linear) {
                                    listVM.updateItem(item: item)
                                }
                            }
                    }
                    .onDelete(perform: listVM.removeItem)
                    .onMove(perform: listVM.moveItem)
                    
                }
                .listStyle(.plain)
            }
        }
        .navigationTitle("Todo List 📝")
        .toolbar {
            ToolbarItem(placement:.navigationBarLeading) {
                EditButton()
            }
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    self.isPresented.toggle()
                } label: {
                    Image(systemName: "plus")
                }
            }
        }
        .navigationDestination(isPresented: $isPresented) {
            AddView(isPresented: $isPresented)
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            ListView()
        }
        .environmentObject(ListViewModel())
    }
}


