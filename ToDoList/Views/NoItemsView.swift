//
//  NoItemsView.swift
//  ToDoList
//
//  Created by Muzaffer Çokaslan on 20.09.2023.
//

import SwiftUI

struct NoItemsView: View {
    let secondaryAccentColor:Color = Color("SecondaryAccentColor", bundle: nil)
    @State var animate:Bool = false
    @State var isPresented:Bool = false
    var body: some View {
        ScrollView {
            VStack(spacing: 10 ,content: {
                Text("There are no items!")
                    .font(.title)
                    .fontWeight(.semibold)
                Text("Are you productive person? I think you should click the add button and a bunch of items to your todo list!")
                    .padding(.bottom, 20)
                Button(action: {
                    self.isPresented.toggle()
                }, label: {
                    Text("Add Something")
                        .foregroundStyle(.white)
                        .font(.headline)
                        .frame(height: 55)
                        .frame(maxWidth: .infinity)
                        .background(animate ? secondaryAccentColor : Color.accentColor)
                        .clipShape(RoundedRectangle(cornerSize: CGSize(width:10, height: 10)))
                })
                .padding(.horizontal, animate ? 30 : 50)
                .shadow(
                    color:animate ? secondaryAccentColor .opacity(0.7) : Color.accentColor.opacity(0.7),
                    radius: animate ? 30 : 10,
                    x:0.0,
                    y:animate ? 30 : 10
                )
                .scaleEffect(animate ? 1.1 : 1.0)
                .offset(y:animate ? -7 : 0)
            })
            .frame(maxWidth: 400)
            .multilineTextAlignment(.center)
            .padding(40)
            .onAppear(perform: addAnimation)
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .navigationDestination(isPresented: $isPresented) {
            AddView(isPresented: $isPresented)
        }
    }
    
    func addAnimation(){
        guard !animate else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            withAnimation(Animation.easeInOut(duration: 2.0).repeatForever()) {
                animate.toggle()
            }
        })
    }
}

#Preview {
    NavigationStack {
        NoItemsView()
            .navigationTitle("Title")
    }
}
